--
-- (C) 2020 - ntop.org
--

return {
   network_discovery_description = "Trigger an alert when a Network Discovery is detected",
   network_discovery_title = "Network Discovery Detected",

   network_discovery_alert_description = "Periodic Network Discovery executed",
}
